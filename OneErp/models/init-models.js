var DataTypes = require("sequelize").DataTypes;
var _agents = require("./agents");
var _application = require("./application");
var _applicationmodule = require("./applicationmodule");
var _applicationmoduleaction = require("./applicationmoduleaction");
var _profil = require("./profil");
var _profilapplication = require("./profilapplication");
var _profilmodule = require("./profilmodule");
var _profilmoduleaction = require("./profilmoduleaction");
var _utilisateur = require("./utilisateur");

function initModels(sequelize) {
  var agents = _agents(sequelize, DataTypes);
  var application = _application(sequelize, DataTypes);
  var applicationmodule = _applicationmodule(sequelize, DataTypes);
  var applicationmoduleaction = _applicationmoduleaction(sequelize, DataTypes);
  var profil = _profil(sequelize, DataTypes);
  var profilapplication = _profilapplication(sequelize, DataTypes);
  var profilmodule = _profilmodule(sequelize, DataTypes);
  var profilmoduleaction = _profilmoduleaction(sequelize, DataTypes);
  var utilisateur = _utilisateur(sequelize, DataTypes);
  utilisateur.belongsTo(agents, { as: "id_parent_agent", foreignKey: "id_parent"});
  agents.hasMany(utilisateur, { as: "utilisateurs", foreignKey: "id_parent"});
  applicationmodule.belongsTo(application, { as: "id_application_application", foreignKey: "id_application"});
  application.hasMany(applicationmodule, { as: "applicationmodules", foreignKey: "id_application"});
  profilapplication.belongsTo(application, { as: "id_application_application", foreignKey: "id_application"});
  application.hasMany(profilapplication, { as: "profilapplications", foreignKey: "id_application"});
  applicationmoduleaction.belongsTo(applicationmodule, { as: "id_applicationmodule_applicationmodule", foreignKey: "id_applicationmodule"});
  applicationmodule.hasMany(applicationmoduleaction, { as: "applicationmoduleactions", foreignKey: "id_applicationmodule"});
  profilmodule.belongsTo(applicationmodule, { as: "id_applicationmodule_applicationmodule", foreignKey: "id_applicationmodule"});
  applicationmodule.hasMany(profilmodule, { as: "profilmodules", foreignKey: "id_applicationmodule"});
  // agents.belongsTo(codesociale, { as: "id_codesociale_codesociale", foreignKey: "id_codesociale"});
  // codesociale.hasMany(agents, { as: "agents", foreignKey: "id_codesociale"});
  // agents.belongsTo(dossiercomptable, { as: "id_dossier_dossiercomptable", foreignKey: "id_dossier"});
  // dossiercomptable.hasMany(agents, { as: "agents", foreignKey: "id_dossier"});
  // agents.belongsTo(etatcivil, { as: "id_etatcivil_etatcivil", foreignKey: "id_etatcivil"});
  // etatcivil.hasMany(agents, { as: "agents", foreignKey: "id_etatcivil"});
  // agents.belongsTo(familleagent, { as: "id_famille_familleagent", foreignKey: "id_famille"});
  // familleagent.hasMany(agents, { as: "agents", foreignKey: "id_famille"});
  // agents.belongsTo(gouvernera, { as: "id_gouvn_gouvernera", foreignKey: "id_gouvn"});
  // gouvernera.hasMany(agents, { as: "agents", foreignKey: "id_gouvn"});
  // agents.belongsTo(gouvernera, { as: "lieun_gouvernera", foreignKey: "lieun"});
  // gouvernera.hasMany(agents, { as: "lieun_agents", foreignKey: "lieun"});
  // agents.belongsTo(motifabsenceinactive, { as: "id_motifabsence_motifabsenceinactive", foreignKey: "id_motifabsence"});
  // motifabsenceinactive.hasMany(agents, { as: "agents", foreignKey: "id_motifabsence"});
  // agents.belongsTo(niveaueducatif, { as: "id_niveaueducatif_niveaueducatif", foreignKey: "id_niveaueducatif"});
  // niveaueducatif.hasMany(agents, { as: "agents", foreignKey: "id_niveaueducatif"});
  // agents.belongsTo(niveauxage, { as: "id_niveauxage_niveauxage", foreignKey: "id_niveauxage"});
  // niveauxage.hasMany(agents, { as: "agents", foreignKey: "id_niveauxage"});
  // agents.belongsTo(pays, { as: "id_pays_pay", foreignKey: "id_pays"});
  // pays.hasMany(agents, { as: "agents", foreignKey: "id_pays"});
  profilapplication.belongsTo(profil, { as: "id_profil_profil", foreignKey: "id_profil"});
  profil.hasMany(profilapplication, { as: "profilapplications", foreignKey: "id_profil"});
  utilisateur.belongsTo(profil, { as: "id_profil_profil", foreignKey: "id_profil"});
  profil.hasMany(utilisateur, { as: "utilisateurs", foreignKey: "id_profil"});
  profilmodule.belongsTo(profilapplication, { as: "id_profilapplication_profilapplication", foreignKey: "id_profilapplication"});
  profilapplication.hasMany(profilmodule, { as: "profilmodules", foreignKey: "id_profilapplication"});
  profilmoduleaction.belongsTo(profilmodule, { as: "id_profilmodule_profilmodule", foreignKey: "id_profilmodule"});
  profilmodule.hasMany(profilmoduleaction, { as: "profilmoduleactions", foreignKey: "id_profilmodule"});
  // agents.belongsTo(regroupementagents, { as: "id_regrouppement_regroupementagent", foreignKey: "id_regrouppement"});
  // regroupementagents.hasMany(agents, { as: "agents", foreignKey: "id_regrouppement"});
  // agents.belongsTo(sexe, { as: "id_sexe_sexe", foreignKey: "id_sexe"});
  // sexe.hasMany(agents, { as: "agents", foreignKey: "id_sexe"});
  // agents.belongsTo(typeagents, { as: "id_type_typeagent", foreignKey: "id_type"});
  // typeagents.hasMany(agents, { as: "agents", foreignKey: "id_type"});
  // agents.belongsTo(typepermis, { as: "id_type_permis_typepermi", foreignKey: "id_type_permis"});
  // typepermis.hasMany(agents, { as: "agents", foreignKey: "id_type_permis"});
  // agents.belongsTo(typepermis, { as: "id_typepermis_typepermi", foreignKey: "id_typepermis"});
  // typepermis.hasMany(agents, { as: "id_typepermis_agents", foreignKey: "id_typepermis"});

  return {
    agents,
    application,
    applicationmodule,
    applicationmoduleaction,
    profil,
    profilapplication,
    profilmodule,
    profilmoduleaction,
    utilisateur,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
