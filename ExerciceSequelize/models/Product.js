const { DataTypes } = require('sequelize');
const sequelize = require('../config/database'); // Import your Sequelize configuration
const shortid = require('shortid');
const Product = sequelize.define('product', {
  libelle: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  price: {
    type: DataTypes.DOUBLE,
    allowNull: true
  },
  slug: {
    type: DataTypes.STRING,
    unique: true, // Ensure slugs are unique
  },
});
Product.beforeCreate((product, options) => {
  if (product.libelle) {
    product.slug = shortid.generate();
  }
});
Product.associate = (models) => {
    Product.hasMany(models.ProductVariant, { foreignKey: 'product_id' });
  };
module.exports = Product;