const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('profilmoduleaction', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    libelle: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    id_profilmodule: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'profilmodule',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'profilmoduleaction',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "fki_profilmodule_id_profilmodule",
        fields: [
          { name: "id_profilmodule" },
        ]
      },
      {
        name: "profilmoduleaction_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
