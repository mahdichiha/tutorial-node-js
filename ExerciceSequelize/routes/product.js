// routes/products.js
const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/product');
const authenticateUser = require('../middleware/auth');
// Define routes for product CRUD
// router.get('/', ProductController.getAllProducts);
// router.get('/:id', ProductController.getProductById);
router.post('/', authenticateUser,ProductController.createProduct);
// router.put('/:id', ProductController.updateProduct);
// router.delete('/:id', ProductController.deleteProduct);

module.exports = router;