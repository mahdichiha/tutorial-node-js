const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('profilmodule', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    id_profilapplication: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'profilapplication',
        key: 'id'
      }
    },
    id_applicationmodule: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'applicationmodule',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'profilmodule',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "fki_profilapplication_id_profilapplication",
        fields: [
          { name: "id_profilapplication" },
        ]
      },
      {
        name: "profilmodule_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
