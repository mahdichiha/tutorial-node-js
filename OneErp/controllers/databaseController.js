const { Sequelize } = require('sequelize');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const pepper = 'fnmdcm2010*101*5#mahdi';
const jwt = require('jsonwebtoken');
// require('dotenv').config();
// const sequelize = new Sequelize({
//     dialect: process.env.DB_DIALECT, 
//     host: process.env.DB_HOST,
//     port: process.env.DB_PORT,
//     database: process.env.DB_DATABASE,
//     username: process.env.DB_USERNAME,
//     password: process.env.DB_PASSWORD,
//   });
const config=require('../config/config.json');
const initModels = require('../models/init-models');
async function checkDatabaseConnection(req, res) {
  try {
    const {client_db}= req.body;
    for (const key in config) {
        if (key === client_db) {
          const cbbcConfig = config[key];
          const username = cbbcConfig.username;
          const password = cbbcConfig.password;
          const database = cbbcConfig.database;
          const host = cbbcConfig.host;
          const dialect = cbbcConfig.dialect;
          const port = cbbcConfig.port;
          sequelize = new Sequelize({
            dialect: dialect, 
            host: host,
            port: port,
            database: database,
            username: username,
            password: password,
          });
            await sequelize.authenticate();
           
          return   res.status(200).json({ message: 'Connection has been established successfully.' });
        }
    }
 return   res.status(200).json({ error: 'Customer not found.' });
  } catch (error) {
   return res.status(500).json({ error: 'Unable to connect to the database', details: error.message });
  }
}
async function AuthentificateDb(client_db,login,pwd) {
 
 
    for (const key in config) {
      
        if (key === client_db) {
          const cbbcConfig = config[key];
          const username = cbbcConfig.username;
          const password = cbbcConfig.password;
          const database = cbbcConfig.database;
          const host = cbbcConfig.host;
          const dialect = cbbcConfig.dialect;
          const port = cbbcConfig.port;
          const sequelize = new Sequelize({
            dialect: dialect, 
            host: host,
            port: port,
            database: database,
            username: username,
            password: password,
          });
          try {
            await sequelize.authenticate();
            console.log('Connection has been established successfully.');
            models=initModels(sequelize);
            
            const user = await models.utilisateur.findOne({ where: { login: login } });
            if (user) {
                      
              const spawn = require('child_process').spawn;
              const child = spawn('php', ['decreptage.php']);
            
              child.stdin.setEncoding('utf-8');
              child.stdout.pipe(process.stdout);
            
              child.stdin.write('{"pwd":"'+pwd+'","password":"'+user.password+'"}' + '\n');
             const result=new Promise(resolve=>{
                child.stdout.addListener('data',function(res){
                 resolve(res.toString())
              })
             })
              child.stdin.end();
              const res=await result
           
                if(res){
                    
                  const token = jwt.sign({ userId: user.id }, 'oneerp_key', { expiresIn: '1h' });
                  return  {status:true, token:token,message:'token' };
                 
                } else {
                  return {status:false, message: 'Password incorrect',token:null };
                } // Hash with bcrypt, similar to PHP
               
             
            
            
            
            } else {
              return {status:false, message: 'Invalid user',token:null };
            }
                 
                  

                  
          } catch (error) {
            console.error('Unable to connect to the database:', error);
            throw error; // You might want to handle this error differently
          }
        
        }
      }
   

  
}
module.exports = AuthentificateDb ;
