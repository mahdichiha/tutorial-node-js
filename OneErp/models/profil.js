const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('profil', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    libelle: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'profil',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "profil_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
