const express = require('express');
const app = express();
const port = 3000;
const products = require('./data/product.json');
// CORS
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
      res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
      return res.status(200).json({});
    }
    next();
  });
app.get('/products', (req, res) => {
    
    res.json(products); // Render JSON data
  });

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
  });