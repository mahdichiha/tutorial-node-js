// models/ProductVariant.js

const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const ProductVariant = sequelize.define('ProductVariant', {
  variantName: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  price: {
    type: DataTypes.DECIMAL(10, 2),
    allowNull: false,
  },
  product_id: {
    type: DataTypes.INTEGER,
    allowNull: true
  }
});

ProductVariant.associate = (models) => {
  ProductVariant.belongsTo(models.Product, { foreignKey: 'id' });
};

module.exports = ProductVariant;
