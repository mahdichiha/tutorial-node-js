const { Pool } = require('pg');

// Create a PostgreSQL connection pool
const pool = new Pool({
  connectionString: 'postgres://postgres:fnmdcm2010@localhost/testnodejs',
});

module.exports = {
  getAllProducts: async () => {
    const client = await pool.connect();
    try {
      const result = await client.query('SELECT * FROM products');
      return result.rows;
    } finally {
      client.release();
    }
  },
  createProduct: async (name,description, price) => {
    const client = await pool.connect();
    try {
      const query = 'INSERT INTO products (name,description, price) VALUES ($1, $2,$3) RETURNING *';
      const values = [name,description, price];
      const result = await client.query(query, values);
      return result.rows[0];
    } finally {
      client.release();
    }
  },
  // Add more methods for creating, updating, and deleting products
};
