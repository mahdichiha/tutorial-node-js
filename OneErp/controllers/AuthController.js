

const AuthentificateDb  = require('../controllers/databaseController')

// const register = async (req, res) => {
//   try {
//     const { username, password } = req.body;

//     // Check if the username already exists
//     const existingUser = await User.findOne({ where: { username } });
//     if (existingUser) {
//       return res.status(400).json({ message: 'Username already exists' });
//     }

//     // Hash the password
//     const hashedPassword = await bcrypt.hash(password, 10);

//     // Create a new user
//     const newUser = await User.create({ username, password: hashedPassword });

//     res.status(201).json({ message: 'User registered successfully' });
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ message: 'Server error' });
//   }
// };

const login = async (req, res) => {
  try {
  
    const { login, pwd,client_db } = req.body;
    const {status,message,token}= await AuthentificateDb(client_db,login,pwd);
    
    return  res.status(200).json({ message: message,status:status,token:token });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Server error' });
  }
};

module.exports = {
//   register,
  login,
};
