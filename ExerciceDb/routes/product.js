const express = require('express');
const router = express.Router();
const ProductModel = require('../models/product');

// Define API endpoints
router.get('/', async (req, res) => {
  try {
    const products = await ProductModel.getAllProducts();
    res.json(products);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});
// Add a new product
router.post('/create', async (req, res) => {
    try {
      console.log(req.body);
      const { name,description, price } = req.body;
      // Validate and sanitize the input as needed
  
      // Create a new product in the database
      const newProduct = await ProductModel.createProduct(name,description, price);
  
      res.status(201).json(newProduct);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error'+error+'---'+req.body });
    }
  });

// Add more endpoints for creating, updating, and deleting products

module.exports = router;
