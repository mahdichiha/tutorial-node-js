
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('utilisateur', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    login: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    pwd: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    id_parent: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'agents',
        key: 'id'
      }
    },
    id_profil: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'profil',
        key: 'id'
      }
    },
    remember_token: {
      type: DataTypes.STRING,
      allowNull: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: true
    },
    is_admin: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    id_magasin: {
      type: DataTypes.STRING,
      allowNull: true
    },
    is_superadmin: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    delated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    docscan: {
      type: DataTypes.STRING,
      allowNull: true
    },
    vailderegrouppement: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    is_cheflabo: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    is_chefprojet: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    id_labo: {
      type: DataTypes.STRING,
      allowNull: true
    },
    id_projet: {
      type: DataTypes.STRING,
      allowNull: true
    },
    id_emplacement: {
      type: DataTypes.STRING,
      allowNull: true
    },
    id_labs: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'utilisateur',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "id_parent",
        fields: [
          { name: "id_parent" },
        ]
      },
      {
        name: "utilisateur_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
