const { json } = require('sequelize');
const Product=require('../models/Product');
const createProduct = async (req, res) => {
    try {
      const {libelle, description, price}=req.body;
      const  check_product=Product.findOne({libelle:libelle})
      if(!check_product){
        Product.create({
            libelle: libelle,
            description: description,
            price: price,
          })
          res.status(200).json({ message: 'product created' });
      }
      res.status(200).json({ message: 'product exist' });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'error creation'+error });
    }
  };
  
  module.exports = {createProduct };