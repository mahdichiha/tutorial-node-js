const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('application', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    libelle: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'application',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "application_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
