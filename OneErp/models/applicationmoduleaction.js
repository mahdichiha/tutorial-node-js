const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('applicationmoduleaction', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    libelle: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    id_applicationmodule: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'applicationmodule',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'applicationmoduleaction',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "applicationmoduleaction_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "fki_applicationmodule_id_applicationmodule",
        fields: [
          { name: "id_applicationmodule" },
        ]
      },
    ]
  });
};
