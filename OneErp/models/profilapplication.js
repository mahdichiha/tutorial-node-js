const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('profilapplication', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    id_profil: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'profil',
        key: 'id'
      }
    },
    id_application: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'application',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'profilapplication',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "fki_application_id_application",
        fields: [
          { name: "id_application" },
        ]
      },
      {
        name: "fki_profil_id_profil",
        fields: [
          { name: "id_profil" },
        ]
      },
      {
        name: "profilapplication_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
