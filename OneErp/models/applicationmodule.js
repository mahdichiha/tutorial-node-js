const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('applicationmodule', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    libelle: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    id_application: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'application',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'applicationmodule',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "applicationmodule_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "fki_id_application",
        fields: [
          { name: "id_application" },
        ]
      },
    ]
  });
};
