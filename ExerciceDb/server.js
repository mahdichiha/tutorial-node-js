const express = require('express');
const app = express();
const productRoutes = require('./routes/product');
app.use(express.json());
app.use((req, res, next) => {
    try{
        res.header("Access-Control-Allow-Origin", "*");
        res.header(
          "Access-Control-Allow-Headers",
          "Origin, X-Requested-With, Content-Type, Accept, Authorization"
        );
        if (req.method === "OPTIONS") {
          res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
          return res.status(200).json({});
        }
        next();
    }
   
    catch (error) {
        console.error("CORS Middleware Error: ", error);
        next(error); // Pass the error to the error handler
    }
  });
app.use('/products', productRoutes);
app.use('/create', productRoutes);

app.listen(3000, () => {
  console.log('Server is running on port 3000');
});
