
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('agents', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nomcomplet: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    idrh: {
      type: DataTypes.STRING,
      allowNull: true
    },
    cin: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    gsm: {
      type: DataTypes.STRING(22),
      allowNull: true
    },
    mail: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    id_famille: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'familleagent',
        key: 'id'
      }
    },
    prenom: {
      type: DataTypes.STRING,
      allowNull: true
    },
    daten: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    id_gouvn: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'gouvernera',
        key: 'id'
      }
    },
    prenompere: {
      type: DataTypes.STRING,
      allowNull: true
    },
    prenomgp: {
      type: DataTypes.STRING,
      allowNull: true
    },
    nompmere: {
      type: DataTypes.STRING,
      allowNull: true
    },
    codepostal: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    longeur: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    idpersonnel: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    cip: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    dates: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    photo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    accuetiv: {
      type: DataTypes.STRING,
      allowNull: true
    },
    id_type_permis: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'typepermis',
        key: 'id'
      }
    },
    id_niveauxage: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'niveauxage',
        key: 'id'
      }
    },
    datenaissance: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    lieunaissance: {
      type: DataTypes.STRING,
      allowNull: true
    },
    id_niveaueducatif: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'niveaueducatif',
        key: 'id'
      }
    },
    id_sexe: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'sexe',
        key: 'id'
      }
    },
    id_etatcivil: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'etatcivil',
        key: 'id'
      }
    },
    adresse: {
      type: DataTypes.STRING,
      allowNull: true
    },
    etatmulitaire: {
      type: DataTypes.CHAR(255),
      allowNull: true
    },
    id_type: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'typeagents',
        key: 'id'
      }
    },
    id_pays: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'pays',
        key: 'id'
      }
    },
    dateaffiliation: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    nbrenfants: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    cheffamille: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    id_gouvernerat: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    lieun: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'gouvernera',
        key: 'id'
      }
    },
    age: {
      type: DataTypes.CHAR(25),
      allowNull: true
    },
    idcnss: {
      type: DataTypes.CHAR(25),
      allowNull: true
    },
    regroupement: {
      type: DataTypes.CHAR(35),
      allowNull: true
    },
    id_regrouppement: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'regroupementagents',
        key: 'id'
      }
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    id_motifabsence: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'motifabsenceinactive',
        key: 'id'
      }
    },
    datesortie: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    id_codesociale: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'codesociale',
        key: 'id'
      }
    },
    id_typepermis: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'typepermis',
        key: 'id'
      }
    },
    rib: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    id_dossier: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'dossiercomptable',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'agents',
    schema: 'public',
    hasTrigger: true,
    timestamps: false,
    indexes: [
      {
        name: "agents_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
