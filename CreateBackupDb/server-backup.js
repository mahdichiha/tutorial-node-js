const { exec } = require('child_process');
const backupFileName = 'backup.sql';

const cmd = `pg_dump -U postgres -d cbbc_final -f "${backupFileName}"`;

const child = exec(cmd, { env: { PGPASSFILE:   '/.pgpass' } });

child.stdout.on('data', (data) => {
  console.log(data);
});

child.stderr.on('data', (data) => {
  console.error(data);
});

child.on('exit', (code) => {
  if (code === 0) {
    console.log('Database backup completed successfully.');
  } else {
    console.error('Error:', code);
  }
});
